all:
	g++-12 -std=c++20 -ffast-math -march=native -O3 -I. l2.cpp -o l2

clean:
	rm -rf l2
