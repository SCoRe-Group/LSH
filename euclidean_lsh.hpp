/***
 *  $Id$
 **
 *  File:
 *  Created: Dec 19, 2014
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2014-2022 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */


#ifndef EUCLIDEAN_LSH_HPP
#define EUCLIDEAN_LSH_HPP

#include <algorithm>
#include <cmath>
#include <deque>
#include <numbers>
#include <random>
#include <unordered_map>
#include <vector>

#include "external/jaz/hash.hpp"


// This code is based on:
//
// [1] Andoni, A. and Indyk, P. "Near-optimal Hashing Algorithms for Approximate
//     Nearest Neighbor in High Dimensions"
//
// [2] Dasgupta, A. and Kumar, R. and Sarlos, T. "Fast Locality-sensitive Hashing."
//
// [3] Datar, M. and Indyk, P. and Immorlica, N. and Mirrokni, V. "Locality-sensitive
//     Hashing Scheme Based on p-Stable Distributions"
namespace elsh {

  template <typename RandomDist = std::normal_distribution<>> class LSH_Function {
  public:
      explicit LSH_Function(int d = 0) : a_(d), b_(d), w_(0) { }

      template <typename Random> void init(double w, Random&& rng) {
          RandomDist dist(0, 1);
          std::generate(std::begin(a_), std::end(a_), [&](){ return dist(rng); });

          std::uniform_real_distribution<> u_dist(0, w);
          b_ = u_dist(rng);
          w_ = w;
      } // init

      template <typename Sequence> int operator()(const Sequence& x) const noexcept {
          double S = std::inner_product(std::begin(a_), std::end(a_), std::begin(x), 0.0);
          return std::floor((S + b_) / w_);
      } // operator()

  protected:
      std::vector<double> a_;
      double b_;
      double w_;

  }; // class LSH_Function

  template <typename RandomDist, typename Random>
  inline LSH_Function<RandomDist> make_lsh_function(int d, int w, Random&& rng) {
      LSH_Function<RandomDist> h(d);
      h.init(w, rng);
      return h;
  } // make_lsh_function


  using d1D = std::cauchy_distribution<>;
  using LSH_1D = LSH_Function<d1D>;

  template <typename Random> inline LSH_1D make_lsh_1D(int d, int w, Random&& rng) {
      return make_lsh_function<d1D, Random>(d, w, rng);
  } // make_lsh_1D


  using d2D = std::normal_distribution<>;
  using LSH_2D = LSH_Function<d2D>;

  template <typename Random> inline LSH_2D make_lsh_2D(int d, int w, Random&& rng) {
      return make_lsh_function<d2D, Random>(d, w, rng);
  } // make_lsh_2D


  template <typename RandomDist> class Euclidean_LSH {
  public:
      template <typename Random> void init(int d, int L, int k, int w, Random&& rng) {
          if (k > MAX_K) throw std::runtime_error("Invalid parameter in Euclidean_LSH");

          d_ = d;
          L_ = L;
          k_ = k;
          w_ = w;

          H_.resize(L * k);
          for (auto& x : H_) x = make_lsh_function<RandomDist>(d, w, rng);
      } // init

      template <typename Sequence>
      void g(int j, const Sequence& x, int* gj) const noexcept {
          int pos = j * k_;
          for (int i = 0; i < k_; ++i) gj[i] = H_[pos + i](x);
      } // g

      template <typename Sequence>
      std::vector<uint64_t> operator()(const Sequence& x) const noexcept {
          std::vector<uint64_t> res(L_);
          int gj[MAX_K];

          const char* ptr = reinterpret_cast<const char*>(gj);
          int l = k_ * sizeof(decltype(gj[0]));

          for (int j = 0; j < L_; ++j) {
              g(j, x, gj);
              res[j] = hash_(ptr, l);
          }

          return res;
      } // operator()

      template <typename Sequence>
      void operator()(const Sequence& x, std::vector<uint64_t>& res) const noexcept {
          int gj[MAX_K];
          res.resize(L_);

          const char* ptr = reinterpret_cast<const char*>(gj);
          int l = k_ * sizeof(decltype(gj[0]));

          for (int j = 0; j < L_; ++j) {
              g(j, x, gj);
              res[j] = hash_(ptr, l);
          }
      } // operator()

  private:
      static const int MAX_K = 16;

      std::vector<LSH_Function<RandomDist>> H_;

      // seems there is no real difference between performance of the two?
      jaz::murmur64A hash_;
      //jaz::fnv1a hash_;

      int d_;
      int L_;
      int k_;
      int w_;

  }; // class Euclidean_LSH


  template <typename RandomDist, typename Random>
  inline Euclidean_LSH<RandomDist> make_euclidean_lsh(int d, int L, int k, int w, Random&& rng) {
      Euclidean_LSH<RandomDist> H;
      H.init(d, L, k, w, rng);
      return H;
  } // make_euclidean_lsh


  double inline N01_cdf__(double x) { return 0.5 * (1.0 + std::erf(x / std::sqrt(2.0))); }

  // Probability that two vectors collide under a hash function drawn uniformly at random
  // from LSH family (see Section 3.1 in [3]). This probability, p(c), can be used to
  // assess L and k when deciding on hashing scheme (see below).
  template <typename RandomDist>
  inline double pcollision(double w, double c = 1.0) { return 0.0; }

  template <> inline double pcollision<d1D>(double w, double c) {
      return (2.0 * std::atan(w / c)) / std::numbers::pi - (std::log(1.0 + ((w * w) / (c * c))) / ((std::numbers::pi * w) / c));
  } // pcollision

  template <> inline double pcollision<d2D>(double w, double c) {
      return 1 - 2.0 * N01_cdf__(-w / c) - ((2.0 * c) / (std::sqrt(2.0 * std::numbers::pi) * w)) * (1.0 - std::exp(-(w * w) / (2.0 * c * c)));
  } // pcollision


  // We want near neighbors to be reported with probability at least 1 - sigma.
  template <typename RandomDist = d2D>
  inline int lestimate(double R, double sigma, int k, double w) {
      double p1 = pcollision<RandomDist>(w, R);
      return std::ceil(std::log(sigma) / std::log(1.0 - std::pow(p1, k)));
  } // lestimate


  // Class: LSH_Store
  // Simple implementation of LSH database
  // for high dimensional data under L1 or L2 norm.
  template <typename RandomDist, typename Sequence = std::vector<double>>
  class LSH_Store {
  private:
      struct s_ident_hash__ {
          auto operator()(uint64_t x) const { return x; }
      }; // struct s_ident_hash__

  public:
      using hash_table = std::unordered_multimap<uint64_t, int, s_ident_hash__>;

      // Function: init
      // Initialize memory and hashing functions for the database.
      // This method must be called to make database usable.
      // If <insert> or <query> is called before <init>, the behavior is undefined.
      //
      // Parameters:
      // d - Data dimension.
      // L - Number of hash tables to use.
      // k - Number of LSH functions to concatenate.
      // w - Parameter w of LSH functions (number of segments).
      //     The recommended value is 4 (see [2] and [3]).
      //
      // Note: Parameters L and k should be selected based on the radius R
      // and the recall \sigma. See function: <lestimate>.
      template <typename Random>
      void init(int d, int L, int k, int w, Random&& rng) {
          index_.resize(L);
          H_ = make_euclidean_lsh<RandomDist, Random>(d, L, k, w, rng);
          h_.resize(L);
          L_ = L;
      } // init

      // Function: insert
      // Adds point to the database.
      void insert(const Sequence& x) {
          int pos = db_.size();
          db_.push_back(x);

          H_(x, h_);

          for (int i = 0; i < L_; ++i) index_[i].insert({h_[i], pos});
      } // insert

      // Function: query
      // Returns:
      //   a list of points colliding with x. These points, with high
      //   probability, are within the radius R from x.
      std::vector<int> query(const Sequence& x) {
          std::vector<int> points;
          H_(x, h_);

          for (int i = 0; i < L_; ++i) {
              auto res = index_[i].equal_range(h_[i]);
              for (; res.first != res.second; ++res.first) {
                  auto pos = std::lower_bound(std::begin(points), std::end(points), res.first->second);
                  if ((pos == std::end(points)) || (*pos != res.first->second)) points.insert(pos, res.first->second);
              }
          }

          return points;
      } // query

      // Function: point
      // Returns:
      //   a reference to the point i stored in the database.
      const Sequence& point(int i) const { return db_[i]; }

      // Function: table
      // Returns:
      //   a reference to the table i (out of L tables) maintaining LSH buckets.
      const hash_table& table(int i) const { return index_[i]; }

      // Function: size
      // Returns:
      //   the number of points stored in the database.
      auto size() const { return index_.size(); }


  private:
      std::vector<hash_table> index_;
      std::deque<Sequence> db_;
      int L_;

      Euclidean_LSH<RandomDist> H_;
      std::vector<uint64_t> h_;

  }; // class LSH_Store

} // namespace elsh

#endif // EUCLIDEAN_LSH_HPP
