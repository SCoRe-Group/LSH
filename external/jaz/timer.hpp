/***
 *  $Id$
 **
 *  File: timer.hpp
 *  Created: Oct 16, 2020
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2020 Jaroslaw Zola
 *  Distributed under the Boost Software License, Version 1.0.
 *  See accompanying file LICENSE_BOOST.txt.
 *
 *  This file is part of jaz.
 */

#ifndef TIMER_HPP
#define TIMER_HPP

#include <chrono>


namespace jaz {

  class timer {
  public:
      timer() { reset(); }

      void reset() {
          tp_ = std::chrono::steady_clock::now();
      } // reset

      double elapsed() const {
          auto tc = std::chrono::steady_clock::now();
          std::chrono::duration<double> diff = tc - tp_;
          return diff.count();
      } // elapsed

  private:
      std::chrono::steady_clock::time_point tp_;

  }; // class timer

} // namespace jaz

#endif // TIMER_HPP
