# Euclidean LSH

This is simple, but robust, implementation of the classic LSH scheme for L1 and L2 norms. Implementation is based on a series of papers by Andoni, Indyk and others (see the source code for details).

This is a single header C++20 library (see [`euclidean_lsh.hpp`](euclidean_lsh.hpp)). You can find a complete use example in [`l2.cpp`](l2.cpp) together with the corresponding `Makefile`.
