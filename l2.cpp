#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

#include "external/jaz/algorithm.hpp"
#include "external/jaz/timer.hpp"

#include "euclidean_lsh.hpp"


template <typename T>
void print_vector(std::ostream& os, const std::vector<T>& v) {
    for (auto x : v) os << " " << x;
} // print_vector


double l2(const std::vector<double>& u, const std::vector<double>& v) {
    double S = 0.0;
    int d = u.size();
    for (int i = 0; i < d; ++i) S += ((u[i] - v[i]) * (u[i] - v[i]));
    return std::sqrt(S);
} // l2


int main(int argc, char* argv[]) {
    int D = 10;      // dimensions
    int n = 1000000; // number of points to insert

    std::random_device rd;
    std::mt19937 rng(1);

    std::uniform_real_distribution<> udist(-10, 10);

    // create LSH table with k=8 and w=4 and estimated L
    // we want to extract points within R = 0.75 from query
    // with probability at least 1 - sigma
    double R = 0.75;
    double sigma = 0.1;

    int k = 8;
    int w = 4;

    // to work with L1 use elsh::d1D
    auto L = elsh::lestimate<elsh::d2D>(R, sigma, k, w);
    //auto L = 8;

    std::cout << "Estimated L=" << L << " "
              << "with k=" << k << ", "
              << "w=" << w << ", "
              << "R=" << R << ", "
              << "sigma=" << sigma << std::endl;

    elsh::LSH_Store<elsh::d2D> db;
    db.init(D, L, k, w, rng);

    std::cout << "DB initialized" << std::endl;
    std::cout << "Inserting " << n << " random points..." << std::endl;

    std::vector<double> x(D);

    jaz::timer T;

    for (int i = 0; i < n; ++i) {
        std::generate(std::begin(x), std::end(x), [&](){ return udist(rng); });
        db.insert(x);
    }

    std::cout << "Done in " << T.elapsed() << "s" << std::endl;

    std::cout << "Creating query point from point " << (n >> 1) << "..." << std::endl;

    auto qp = db.point(n >> 1);
    for (auto& qx : qp) qx += (udist(rng) / 100.0);

    db.insert(qp);

    std::cout << "Query point:" << std::endl;
    std::cout << "  ";
    print_vector(std::cout, qp);
    std::cout << std::endl;

    std::cout << "Running query..." << std::endl;

    auto res = db.query(qp);

    std::cout << "Found " << res.size() << " hit(s):" << std::endl;

    for (auto x : res) {
        std::cout << "   Point: " << x << "\n";
        std::cout << "   L2 from query: " << l2(qp, db.point(x)) << "\n";
        std::cout << "  ";
        print_vector(std::cout, db.point(x));
        std::cout << std::endl;
    }

    return 0;

    ///* Example of how to access internal data of the store
    // get table with id 0 (we have L tables total)
    int t = 0;
    auto tab = db.table(t);

    // get number of buckets in the table
    int buckets = tab.bucket_count();

    std::cout << std::endl;
    std::cout << "Table " << t << ", buckets: " << buckets << std::endl;

    // iterate over buckets
    for (int i = 0; i < buckets; ++i) {
        if (tab.bucket_size(i) > 0) {
            // iterate over elements in the bucket i
            auto iter = tab.begin(i);
            auto last = tab.end(i);
            // iter->first is the hash value of the point
            // iter->second is the id of the point in the bucket
            // to access the point use:
            // auto x = db.point(iter->second);
            // D == x.size(), x[0],...,x[D-1]

            std::cout << "Bucket: " << i << ", size: " << tab.bucket_size(i) << std::endl;
            if (tab.bucket_size(i) < 2) continue;

            /*
            for (; iter != last; ++iter) {
                std::cout << "   " << iter->first << " " << iter->second << std::endl;
            }
            */

            while (iter != last) {
                auto tmp = jaz::range(iter, last, [](decltype(*iter) x, decltype(*iter) y) { return x.first == y.first; });
                if (std::distance(iter, tmp) > 1) {
                    std::cout << "collision!" << std::endl;
                    for (; iter != tmp; ++iter) {
                        std::cout << "   " << iter->first << " " << iter->second << std::endl;
                    }
                }
                iter = tmp;
            }

        } // if
    } // for i
    //*/

    std::_Exit(0);
    return 0;
} // main
